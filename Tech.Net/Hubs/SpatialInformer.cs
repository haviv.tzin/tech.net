﻿using System;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Tech.Net.DataService;

namespace Tech.Net.Hubs
{
    public class SpatialInformer
    {
        // Singleton instance
        private static readonly Lazy<SpatialInformer> SpatialInformerInstance = new Lazy<SpatialInformer>(
            () => new SpatialInformer(GlobalHost.ConnectionManager.GetHubContext<SpatialInfoHub>().Clients));

        public static SpatialInformer Instance => SpatialInformerInstance.Value;

       /// <summary>
        /// Ctor 
        /// </summary>
        /// <param name="clients"></param>
        private SpatialInformer(IHubConnectionContext<dynamic> clients)
        {
            Clients = clients;
           // LoadDefaultStocks();
        }


       /// <summary>
       /// All online connected web clients
       /// </summary>
        private IHubConnectionContext<dynamic> Clients
        {
            get;
            set;
        }

       /// <summary>
       ///  A request by the field operator to create new object
       /// </summary>
       public void BroadcastObjectChanged(SpatialObject spatialObject)
       {
           Clients.All.updateObjectData(spatialObject);
       }

        /// <summary>
        ///  A request by the field operator to create new object
        /// </summary>
        public void BroadcastNewObject(SpatialObject spatialObject)
       {
           Clients.All.newObject(spatialObject);
       }
    }

}