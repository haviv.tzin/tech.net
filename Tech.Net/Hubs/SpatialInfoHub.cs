﻿using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Tech.Net.DataService;

namespace Tech.Net.Hubs
{
    [HubName("spatialInfo")]
    public class SpatialInfoHub: Hub
    {
        private readonly IReadRepository _readRepository = HttpContext.Current==null?null: DependencyResolver.Current.GetService<IReadRepository>();
        private readonly IWriteRepository _writeRepository = HttpContext.Current == null ? null : DependencyResolver.Current.GetService<IWriteRepository>();

        private readonly SpatialInformer _spatialInformer;

        /// <summary>
        /// Default ctor
        /// </summary>
        public SpatialInfoHub():this(SpatialInformer.Instance)
        {

        }

        /// <summary>
        /// Internal use ctor
        /// </summary>
        /// <param name="spatialInformer"></param>
        public SpatialInfoHub(SpatialInformer spatialInformer)
        {
            _spatialInformer = spatialInformer;
        }

        /// <summary>
        /// SingalR client call this method request to update Spatial Object data
        /// </summary>
        /// <param name="spatialObject"></param>
        public void UpdateObjectData(SpatialObject spatialObject)
        {
            var contextObject = _writeRepository.UpdateSpatialObject(spatialObject);
            _spatialInformer.BroadcastObjectChanged(contextObject);
        }

        /// <summary>
        /// SingalR client call this method request to create new Spatial Object
        /// </summary>
        /// <param name="spatialObject"></param>
        public void CreateNewObject(SpatialObject spatialObject)
        {
            var contextObject = _writeRepository.AddNewSpatialObject(spatialObject);
            _spatialInformer.BroadcastNewObject(contextObject);
        }
    }
}