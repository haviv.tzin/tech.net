﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using Tech.Net.DataService;
using Tech.Net.Models;

namespace Tech.Net.Controllers
{
    public class SpatialRestServiceController : ApiController
    {

        private readonly IReadRepository _readRepository =  DependencyResolver.Current.GetService<IReadRepository>();
        private readonly IWriteRepository _writeRepository =  DependencyResolver.Current.GetService<IWriteRepository>();

        /// <summary>
        /// Default Ctor
        /// </summary>
        public SpatialRestServiceController()
        {
        }


        [System.Web.Http.HttpPost]
        public IEnumerable<SpatialObject> GetMapBoundsObjects(LatLngBounds bounds)
        {
            var northEast = new Location { Latitude = bounds.NorthEast.Latitude, Longitude = bounds.NorthEast.Longitude };
            var southWest = new Location { Latitude = bounds.SouthWest.Latitude, Longitude = bounds.SouthWest.Longitude };
            var readObjects = _readRepository.GetMapBoundsObjects(northEast, southWest);

             var retVal = readObjects.Select(a =>
            {
                var obj = new SpatialObject();
                obj.CopyFrom(a);
                return obj;
            }).ToList();

            return retVal;
        }

        [System.Web.Http.HttpPost]
        public long GetGeographyDistance(TwoGeoPoints bounds)
        {
            var point1 = new Location { Latitude = bounds.Point1.Latitude, Longitude = bounds.Point1.Longitude };
            var point2 = new Location { Latitude = bounds.Point2.Latitude, Longitude = bounds.Point2.Longitude };

            var retVal = _readRepository.GetGeographyDistance(point1, point2);

            return retVal;
        }

        
        public IEnumerable<SpatialObjectReadModel> GetAllSpatialObject()
        {
            var retVal = _readRepository.GetAllObjects();
            return retVal;
        }

    }
}
