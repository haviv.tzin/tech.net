﻿using System.Web.Mvc;
using Tech.Net.DataService;
using Tech.Net.Models;

namespace Tech.Net.Controllers
{
    public class OfficeController : Controller
    {

        private IWriteRepository _writeRepository;
        private IReadRepository _readRepository;

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="writeRepository"></param>
        /// <param name="readRepository"></param>
        public OfficeController(
            IWriteRepository writeRepository,
            IReadRepository readRepository)
        {
            _writeRepository = writeRepository;
            _readRepository = readRepository;
        }

        // GET: Office
        public ActionResult Index()
        {
            var model = new OfficeViewModel{AllObjects=_readRepository.GetAllObjects()};

            return View(model);
        }

        /// <summary>
        /// GetName
        /// </summary>
        /// <returns></returns>
        public JsonResult GetName()
        {
            return Json(new { name = "Haviv move next" }, JsonRequestBehavior.AllowGet);
        }
    }
}