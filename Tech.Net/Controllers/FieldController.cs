﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tech.Net.DataService;

namespace Tech.Net.Controllers
{
    public class FieldController : Controller
    {
        private IWriteRepository _writeRepository;
        private IReadRepository _readRepository;

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="writeRepository"></param>
        /// <param name="readRepository"></param>
        public FieldController(
            IWriteRepository writeRepository, 
            IReadRepository readRepository)
        {
            _writeRepository = writeRepository;
            _readRepository = readRepository;
        }

        // GET: Field
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// return partial view for entering New Object details
        /// </summary>
        /// <returns></returns>
        public ActionResult NewObject()
        {
            return PartialView(new SpatialObject());
        }

    }
}