﻿using System.Web.Mvc;
using Tech.Net.Models;

namespace Tech.Net.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Portal main menu
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var model = new HomeViewModel();
            return View(model);
        }

        /// <summary>
        /// GoToUserView
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GoToUserView(HomeViewModel model)
        {
            switch (model.UserViewName)
            {
                case "Office User":
                    return RedirectToAction("Index", "Office");

                case "Field User":
                    return RedirectToAction("Index", "Field");

                default:
                    return View("Index", new HomeViewModel());
            }
        }
    }
}