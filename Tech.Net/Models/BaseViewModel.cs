﻿using System.Collections.Generic;
using Tech.Net.DataService;

namespace Tech.Net.Models
{
    public abstract class BaseViewModel:IBaseViewModel
    {
        protected IReadRepository _readRepository;

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="readRepository"></param>
        protected BaseViewModel(IReadRepository readRepository)
        {
            _readRepository = readRepository;
        }

        protected abstract string ViewType { get; }

        /// <summary>
        /// GetMapBoundsObjects
        /// </summary>
        /// <param name="bounds"></param>
        /// <returns></returns>
        public List<SpatialObjectReadModel> GetMapBoundsObjects(LatLngBounds bounds)
        {
            var northEast = new Location{Latitude=bounds.NorthEast.Latitude, Longitude = bounds.NorthEast.Longitude };
            var southWest = new Location { Latitude = bounds.SouthWest.Latitude, Longitude = bounds.SouthWest.Longitude };
            var retVal = _readRepository.GetMapBoundsObjects(northEast, southWest);

            return retVal;
        }
    }
}