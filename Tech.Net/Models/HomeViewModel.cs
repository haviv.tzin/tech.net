﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tech.Net.Models
{
    public class HomeViewModel
    {
        public static SelectList UserViews = new SelectList(new[]{"Office User","Field User"});

        [DisplayName("Logged In As")]
        public string UserViewName { get; set; }
    }
}