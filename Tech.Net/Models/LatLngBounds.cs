﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tech.Net.Models
{
    public class LatLngBounds
    {
        public LatLng NorthEast { get; set; }
        public LatLng SouthWest { get; set; }
    }
}