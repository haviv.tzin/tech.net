﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mvc.JQuery.DataTables;
using Tech.Net.DataService;

namespace Tech.Net.Models
{
    /// <summary>
    /// OfficeViewModel
    /// </summary>
    public class OfficeViewModel : BaseViewModel
    {
        private IWriteRepository _writeRepository;

        /// <summary>
        /// Ctor - use from the controller
        /// </summary>
        /// <param name="writeRepository"></param>
        /// <param name="readRepository"></param>
        public OfficeViewModel(IWriteRepository writeRepository, IReadRepository readRepository) : base(readRepository)
        {
            _writeRepository = writeRepository;
        }

        /// <summary>
        /// Default ctor - use when deserialize 
        /// </summary>
        public OfficeViewModel() : base(DependencyResolver.Current.GetService<IReadRepository>())
        {
        }


        public List<SpatialObjectReadModel> AllObjects { get; set; }

        protected override string ViewType => "OFFICE";
         
    }
}