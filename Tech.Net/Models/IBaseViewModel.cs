﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tech.Net.DataService;

namespace Tech.Net.Models
{
    public interface IBaseViewModel
    {
        List<SpatialObjectReadModel> GetMapBoundsObjects(LatLngBounds bounds);
    }
}