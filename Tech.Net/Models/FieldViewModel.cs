﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tech.Net.DataService;

namespace Tech.Net.Models
{
    public class FieldViewModel:BaseViewModel
    {
        private IWriteRepository _writeRepository;

        /// <summary>
        /// Ctor - use from the controller
        /// </summary>
        /// <param name="writeRepository"></param>
        /// <param name="readRepository"></param>
        public FieldViewModel(IWriteRepository writeRepository, IReadRepository readRepository) : base(readRepository)
        {
            _writeRepository = writeRepository;
        }

        /// <summary>
        /// Default ctor - use when deserialize 
        /// </summary>
        public FieldViewModel() : base(DependencyResolver.Current.GetService<IReadRepository>())
        {
            
        }

        protected override string ViewType => "FIELD";
         
    }
}