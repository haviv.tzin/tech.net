﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tech.Net.Models
{
    public class TwoGeoPoints
    {
        public LatLng Point1 { get; set; }
        public LatLng Point2 { get; set; }
    }
}