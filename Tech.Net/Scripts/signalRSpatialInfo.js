﻿var spatialInformer;
var objectsMap = [];
$(document).ready(function () {
    spatialInformer = $.connection.spatialInfo;

    /*
     * client-side hub methods that call by the server
     */
    $.extend(spatialInformer.client,
        {
            updateObjectData: updateObjectData,
            newObject: newObject
        });


    /*
     * Start SingalR Hub
     */
    $.connection.hub.start()
        .then(function () {
            
        })
        .done(function (viewModel) {
            if (viewModel === null) {
                return;
            }
        });
});

/*
 * when server send update on change on spatialObject data
 */
function updateObjectData(spatialObject) {

    var inMapBounds = isSpatialObjectInMapBound(spatialObject);

    var record = objectsMap[spatialObject.Id];
    if (inMapBounds && !record) {
        newObject(spatialObject);
        return;
    }

    var hasNewLocation =
        record.spatialObject.Location.Latitude !== spatialObject.Location.Latitude ||
        record.spatialObject.Location.Longitude !== spatialObject.Location.Longitude;

    var hasNewDescription = record.spatialObject.Description !== spatialObject.Description;

    if (hasNewLocation) {
        $.toast({
            heading: 'Object Relocated',
            text: JSON.stringify(spatialObject),
            showHideTransition: 'slide',
            icon: 'info'
        });
    }

    if (hasNewDescription) {
        $.toast({
            heading: 'Object Description Edited',
            text: JSON.stringify(spatialObject),
            showHideTransition: 'slide',
            icon: 'info'
        });
    }

    if (!inMapBounds) {
        return;
    }



    if (hasNewLocation) {
        var objLatLng = new google.maps.LatLng(spatialObject.Location.Latitude, spatialObject.Location.Longitude);
        record.marker.setPosition(objLatLng);
    }

     if (hasNewDescription) {
        record.spatialObject.Description = spatialObject.Description;
        $('#objInfo-' + spatialObject.Id ).html(spatialObject.Description);

        record.marker.setMap(null);
        record.marker.setMap(map);

        var contentString = "<B>" + spatialObject.Name + "</B><HR/>" + spatialObject.Description;
        google.maps.event.addListener(record.marker, 'click', function () {
            infowindow.setContent(contentString);
            infowindow.open(map, record.marker);
        });


    }
    record.spatialObject = spatialObject;
}

/*
 * isSpatialObjectInMapBound
 */
function isSpatialObjectInMapBound(spatialObject) {
    var objLatLng = new google.maps.LatLng(spatialObject.Location.Latitude, spatialObject.Location.Longitude);

    var bounds = map.getBounds();
    var inMapBounds = bounds.contains(objLatLng);

    return inMapBounds;
}

/*
 * when server send new created spatialObject 
 */
function newObject(spatialObject) {

    if (viewId === "office") {
        $.toast({
            heading: 'New object created',
            text: JSON.stringify(spatialObject),
            showHideTransition: 'slide',
            icon: 'info'
        });
    }

    var inMapBounds = isSpatialObjectInMapBound(spatialObject);
    if (!inMapBounds) {
        return;
    }
    var objLatLng = new google.maps.LatLng(spatialObject.Location.Latitude, spatialObject.Location.Longitude);


    switch (viewId) {

        case 'office':

            var marker = createMapMarker(spatialObject);
            marker.set("id", spatialObject.Id);
            objectsMap[spatialObject.Id] = { spatialObject: spatialObject, marker: marker };

            break;

        case 'field':

            var marker = createMarker(objLatLng, spatialObject.Name, spatialObject.Description);
            marker.set("id", spatialObject.Id);
            objectsMap[spatialObject.Id] = { spatialObject: spatialObject, marker: marker };

            var newDiv = '<li id="spatialObject' + spatialObject.Id + '"><button id="objInfoBtn' + spatialObject.Id + '" type="button" class="btn btn-info" data-toggle="collapse" data-target="#objInfo-' + spatialObject.Id + '">' + spatialObject.Name + '</button> <div id="objInfo-' + spatialObject.Id + '" class="collapse">' + spatialObject.Description + '</div></li>';
            $('#side_bar').append(newDiv);
            $("#objInfoBtn" + spatialObject.Id).click(function () {
                google.maps.event.trigger(marker, 'click');
            });

            $('#goePoint1').append($('<option>', {
                value: spatialObject.Id,
                text: spatialObject.Name
            }));

            $('#goePoint2').append($('<option>', {
                value: spatialObject.Id,
                text: spatialObject.Name
            }));

            break;
    }
}


/*
 * bind office view element events to handlers
 */
function bindOfficeEvents() {

}

/*
 * bind field view element events to handlers
 */
function bindFieldEvents() {

}
