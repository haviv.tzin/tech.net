﻿
var map = null;
var infowindow;
var viewId = 'field';

$(document).ready(function() {

    $('#dialog').dialog({
        autoOpen: false,
        width: 300,
        height: 300,
        modal: true,
        open: function (event, ui) {
            $(this).load("/Field/NewObject");
        },
        buttons: {
            "Add": function () {
                addNewObject();
                $('#dialog').dialog("close");
            },
            Cancel: function () {

                $('#dialog').dialog("close");
            }
        },
        close: function () {
        }
    });

    $('#addNewObjectBtn').click(function () {
        $('#dialog').dialog('open');
    });

    $('#calcDistanceBtn').click(function () {

        var spatialObjectId1 = $('#goePoint1').val();
        var spatialObjectId2 = $('#goePoint2').val();

        if (!$.isNumeric(spatialObjectId1) || !$.isNumeric(spatialObjectId2)) {
            $("#distCalcResult").html("");
            return;
        }

        if (spatialObjectId1 === spatialObjectId2) {
            $("#distCalcResult").html("Distance: 0 m");
            return;
        }
        var record1 = objectsMap[spatialObjectId1];
        var record2 = objectsMap[spatialObjectId2];


        var point1 = new google.maps.LatLng(record1.spatialObject.Location.Latitude, record1.spatialObject.Location.Longitude);
        var point2 = new google.maps.LatLng(record2.spatialObject.Location.Latitude, record2.spatialObject.Location.Longitude);

        calculateDistance(point1, point2);
    });

});

/*
 * Send request to server to add new spatial object to database and inform other clients
 */
function addNewObject() {
    var name = $("#name").val();
    var description = $("#description").val();
    var mapBounds = map.getBounds();
    var southWest = mapBounds.getSouthWest();
    var northEast = mapBounds.getNorthEast();
    var lat = (northEast.lat() + southWest.lat()) / 2;
    var lng = (northEast.lng() + southWest.lng()) / 2;

    var spatialObject = {
        id: 0,
        name: name,
        description: description,
        locationId: 0,
        location: {
            id: 0,
            latitude: lat,
            longitude: lng
        }
    };

    spatialInformer.server.createNewObject(spatialObject);
}

/*
 * initialize once Map loaded
 */
function initialize() {
    var myWrapper = $("#wrapper");
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        myWrapper.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function (e) {
            // code to execute after transition ends
            google.maps.event.trigger(map, 'resize');
        });
    });


    // create the map
    var myOptions = {
        zoom: 15,
        center: new google.maps.LatLng(-31.946440, 115.821312),
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        },
        navigationControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map_canvas"),
        myOptions);

    google.maps.event.addListener(map, 'click', function () {
        infowindow.close();
    });

    map.addListener('idle', function () {
        getMapBoundsObject();
    });

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });

    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();

        if (places.length === 0) {
            return;
        }

        map.setCenter(places[0].geometry.location);

    });

    infowindow = new google.maps.InfoWindow({
        size: new google.maps.Size(150, 50)
    });
}

/*
 * getMapBoundsObject
 */
function getMapBoundsObject() {

    var mapBounds = map.getBounds();
    var southWest = mapBounds.getSouthWest();
    var northEast = mapBounds.getNorthEast();

    var data = { "NorthEast": { "Latitude": northEast.lat(), "Longitude": northEast.lng() }, "SouthWest": { "Latitude": southWest.lat(), "Longitude": southWest.lng() } };

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/api/SpatialRestService/GetMapBoundsObjects",
        "method": "POST",
        "headers": {
            "content-type": "application/json"
        },
        "processData": false,
        "data": JSON.stringify(data)
    }

    $.ajax(settings).done(function (response) {

        if (!$.isArray(response)) {
            return;
        }

        $('#goePoint1')
            .find('option')
            .remove()
            .end();

        $('#goePoint2')
            .find('option')
            .remove()
            .end();

        // id="spatialObject+' + spatialObject.ID 
        Object.keys(objectsMap).forEach(function(key) {
            var record = objectsMap[key];
            $("#spatialObject" + key).remove();
            record.marker.setMap(null);
        });

        objectsMap = [];
        $.each(response, function (index, spatialObject) {
            newObject(spatialObject);
        });

    });
}

/*
 * calculate Distance between two geography points
 */
function calculateDistance(point1LatLng,point2LatLng) {

    var data = { "Point1": { "Latitude": point1LatLng.lat(), "Longitude": point1LatLng.lng() }, "Point2": { "Latitude": point2LatLng.lat(), "Longitude": point2LatLng.lng() } };
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/api/SpatialRestService/GetGeographyDistance",
        "method": "POST",
        "headers": {
            "content-type": "application/json"
        },
        "processData": false,
        "data": JSON.stringify(data)
    }

    $.ajax(settings).done(function (response) {
        $("#distCalcResult").html("Distance: " + response + " m");
    });

}

// A function to create the marker and set up the event window function 
function createMarker(latLng, name, html) {
    var contentString = "<H3>"+name+"</H3><HR/>"+ html;
    var marker = new google.maps.Marker({
        position: latLng,
        draggable: true,
        map: map,
        zIndex: Math.round(latLng.lat() * -100000) << 5
    });

    google.maps.event.addListener(marker, 'click', function () {
        infowindow.setContent(contentString);
        infowindow.open(map, marker);
    });


    google.maps.event.addListener(marker, 'dragend', function (event) {
        var newLocation = event.latLng;
        var record = objectsMap[marker.get("id")];
        //spatialObject.Location.Latitude, spatialObject.Location.Longitude
        record.spatialObject.Location.Latitude = newLocation.lat();
        record.spatialObject.Location.Longitude = newLocation.lng();
        spatialInformer.server.updateObjectData(record.spatialObject);
    });

    // save the info we need to use later for the side_bar

    return marker;
}


