﻿var gmarkers = [];
var map = null;
var infowindow;
var viewId = 'office';
var selectedSpatialObjectId;
var selectedRow;
/*
 * Initial after doc loaded
 */
$(document).ready(function () {

    pageGrids.spatialObjGrid.onRowSelect(function (e) {
        var row = e.row;
        var spatialObjectId = row.Id;
        if (!row.Location) {
            $.extend(row,
                {
                    Location: { Latitude: row.Latitude, Longitude: row.Longitude }
                });
        }
        var marker = gmarkers[spatialObjectId];
        if (!marker) {
            marker = createMapMarker(row);
        }
        selectedSpatialObjectId = parseInt( spatialObjectId);
        map.setCenter(new google.maps.LatLng(row.Latitude, row.Longitude));
        selectedRow = row;
        $("#objDescription").val(row.Description);
    });

    $("#updateObjDescriptionBtn").click(function() {
        if (!selectedRow) {
            return;
        }
        selectedRow.Description = $("#objDescription").val();
        spatialInformer.server.updateObjectData({ Id: selectedRow.Id, Name: selectedRow.Name, Description: selectedRow.Description });
        $(".grid-cell[data-name='Description']").filter(function() {
            return $(this).parent('.grid-row-selected').length > 0;
        }).html(selectedRow.Description);

        var marker = gmarkers[selectedSpatialObjectId];
        marker.setMap(null);
        marker.setMap(map);

        var contentString = "<B>" + selectedRow.Name + "</B><HR/>" + selectedRow.Description;
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(contentString);
            infowindow.open(map, marker);
        });
        infowindow.setContent(contentString);
        infowindow.open(map, marker);
    });
});


/*
 *  Initial after Map loaded
 */
function initialize() {



    // create the map
    var myOptions = {
        zoom: 15,
        center: new google.maps.LatLng(-31.946440, 115.821312),
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        },
        navigationControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        fullScreenControl:false
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    
    google.maps.event.addListener(map, 'click', function () {
        infowindow.close();
    });

    map.addListener('center_changed', function () {
        getMapBoundsObject();
    });

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });

    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();

        if (places.length === 0) {
            return;
        }

        map.setCenter(places[0].geometry.location);

    });

    infowindow = new google.maps.InfoWindow({
        size: new google.maps.Size(150, 50)
    });
}

/*
 * getMapBoundsObject
 */
function getMapBoundsObject() {

    var mapBounds = map.getBounds();
    var southWest = mapBounds.getSouthWest();
    var northEast = mapBounds.getNorthEast();

    var data = { "NorthEast": { "Latitude": northEast.lat(), "Longitude": northEast.lng() }, "SouthWest": { "Latitude": southWest.lat(), "Longitude": southWest.lng() } };

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/api/SpatialRestService/GetMapBoundsObjects",
        "method": "POST",
        "headers": {
            "content-type": "application/json"
        },
        "processData": false,
        "data": JSON.stringify(data)
    }

    $.ajax(settings).done(function (response) {

        if (!$.isArray(response)) {
            return;
        }

        Object.keys(gmarkers).forEach(function (key) {
            var marker = gmarkers[key];
            marker.setMap(null);
        });

        objectsMap = [];
        gmarkers = [];
        $.each(response, function (index, spatialObject) {
            createMapMarker(spatialObject);
        });

    });
}

/*
 * create marker on map
 */
function createMapMarker(spatialObject) {
    var contentString = "<B>" + spatialObject.Name + "</B><HR/>" + spatialObject.Description;
    var latLng = new google.maps.LatLng(spatialObject.Location.Latitude, spatialObject.Location.Longitude);
    var marker = new google.maps.Marker({
        position: latLng,
        draggable: false,
        map: map,
        zIndex: Math.round(latLng.lat() * -100000) << 5
    });

    google.maps.event.addListener(marker, 'click', function () {
        infowindow.setContent(contentString);
        infowindow.open(map, marker);
    });

    // save the info we need to use later for the side_bar
    gmarkers[spatialObject.Id] = marker;

    if (selectedSpatialObjectId === spatialObject.Id) {
        infowindow.setContent(contentString);
        infowindow.open(map, marker);
    }

    objectsMap[spatialObject.Id] = { spatialObject: spatialObject, marker: marker };

    return marker;
}



