﻿using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Tech.Net.Common;
using Tech.Net.DataService;

namespace Tech.Net
{

    public static class AutofacConfig
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            // autowire
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .As<IIocDependency>()
                .AsImplementedInterfaces()
                .InstancePerDependency();

            builder.RegisterAssemblyTypes(typeof(ReadRepository).Assembly)
                .As<IIocDependency>()
                .AsImplementedInterfaces()
                .InstancePerDependency();

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .As<IIocPerWebRequest>()
                .AsImplementedInterfaces()
                .InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(ReadRepository).Assembly)
                .As<IIocPerWebRequest>()
                .AsImplementedInterfaces()
                .InstancePerRequest();

            // make controllers use constructor injection
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            // change the MVC dependency resolver to use Autofac
            DependencyResolver.SetResolver(new AutofacDependencyResolver(builder.Build()));
        }
    }

}