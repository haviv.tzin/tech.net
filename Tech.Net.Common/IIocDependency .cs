﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tech.Net.Common
{
    /// <summary>
    /// Use by IOC (AutoFac) to register dependencies so that each time they are requested a new instance gets created.
    /// </summary>
    public interface IIocDependency
    {
    }
}
