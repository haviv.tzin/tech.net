﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tech.Net.Common
{
    /// <summary>
    /// Use by IOC (AutoFac) to register dependencies so that only a single instance is instantiated for the duration of the web request
    /// </summary>
    public interface IIocPerWebRequest
    {
    }
}
