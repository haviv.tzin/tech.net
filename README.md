# Tech.Net
       This is Quick and Simple MVC application created to demonstrate code style and usage of the technologies:
            +  C# - OOP
            +  TSql - Relational database, SP, Triggers, Spatial(GEOGRAPHY)
            +  Data - Entity Framework,Linq
            +  Client Side Script - JQuery
            +  Server Side - MVC
            +  Service API - Web API(Rest API)
            +  Real Time - SignalR
            +  Pattern - IoC(Autofac), CQRS, Singlton
            +  API - Google Maps Api
#  Requirements
-  Build geographical base application that  serve in office users and field operators.
-  Any change made by one of the users (Admin, Field operator) must be reflected in real time on screen of all  current users.
-  Field Operator can:
	- Create new object on the map.
	- Change object location.
- Admin can:
	- Edit object details (description).
- A distance tool has to be available for field operator to measure geographical  distance between two objects.

# [ Demo](https://38.132.102.252)

# Deployment Notes
- Update your Google maps api key in cshtml pages
- Create database - Sql scripts enclosed to this project.
