
-- =============================================
-- Author:		Haviv Tzin
-- Description:	Update SpatialObjectReadModel table
-- =============================================
CREATE TRIGGER [dbo].[updateLocationReadModel] 
   ON   [dbo].[Location]
   AFTER  UPDATE
AS 
BEGIN

	SET NOCOUNT ON;


	UPDATE [dbo].[SpatialObjectReadModel]
	   SET [Latitude] = loc.Latitude
		  ,[Longitude] = loc.Longitude
	FROM inserted  loc
	INNER JOIN [dbo].[SpatialObject]  i ON loc.Id = i.LocationId
	 WHERE [dbo].[SpatialObjectReadModel].Id = i.[Id]



END
GO

ALTER TABLE [dbo].[Location] ENABLE TRIGGER [updateLocationReadModel]
GO


