﻿
-- =============================================
-- Author:		Haviv Tzin
-- Description:	Update SpatialObjectReadModel table
-- =============================================
CREATE TRIGGER [dbo].[insertReadModel] 
   ON   [dbo].[SpatialObject]
   AFTER  INSERT
AS 
BEGIN

	SET NOCOUNT ON;


	INSERT INTO [dbo].[SpatialObjectReadModel]
			   ([Id]
			   ,[Name]
			   ,[Description]
			   ,[Latitude]
			   ,[Longitude])
	SELECT i.[Id]
		  ,i.[Name]
		  ,i.[Description]
		  ,loc.Latitude
		  ,loc.Longitude
	FROM inserted  i
	INNER JOIN [dbo].[Location] loc ON loc.Id = i.LocationId


END
GO

ALTER TABLE [dbo].[SpatialObject] ENABLE TRIGGER [insertReadModel]
GO

