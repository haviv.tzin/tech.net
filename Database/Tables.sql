
CREATE TABLE [dbo].[Location](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Latitude] [float] NOT NULL,
	[Longitude] [float] NOT NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



CREATE TABLE [dbo].[SpatialObject](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](250) NULL,
	[LocationId] [int] NOT NULL,
 CONSTRAINT [PK_SpatialObject] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



CREATE TABLE [dbo].[SpatialObjectDocument](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpatialObjectId] [int] NOT NULL,
	[DocName] [nvarchar](100) NOT NULL,
	[DocUrl] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_SpatialObjectDocument] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SpatialObjectReadModel](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](250) NULL,
	[Latitude] [float] NOT NULL,
	[Longitude] [float] NOT NULL,
 CONSTRAINT [PK_SpatialObjectReadModel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


ALTER TABLE [dbo].[SpatialObjectDocument]  WITH CHECK ADD  CONSTRAINT [FK_SpatialObjectDocument_SpatialObject] FOREIGN KEY([SpatialObjectId])
REFERENCES [dbo].[SpatialObject] ([Id])
GO

ALTER TABLE [dbo].[SpatialObjectDocument] CHECK CONSTRAINT [FK_SpatialObjectDocument_SpatialObject]
GO


ALTER TABLE [dbo].[SpatialObject]  WITH CHECK ADD  CONSTRAINT [FK_SpatialObject_Location] FOREIGN KEY([LocationId])
REFERENCES [dbo].[Location] ([Id])
GO

ALTER TABLE [dbo].[SpatialObject] CHECK CONSTRAINT [FK_SpatialObject_Location]
GO


