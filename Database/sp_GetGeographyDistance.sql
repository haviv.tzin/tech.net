
-- =============================================
-- Author:		Haviv Tzin
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetGeographyDistance]
	 @FromLatitude float,
	 @FromLongitude float,
	 @ToLatitude float,
	 @ToLongitude float
AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @FromPoint geography;
	SET @FromPoint = GEOGRAPHY::STGeomFromText('Point(' + CAST(@FromLongitude AS VARCHAR(32)) + ' ' + CAST(@FromLatitude AS VARCHAR(32)) + ')',4326)
	
    DECLARE @ToPoint geography;
	SET @ToPoint = GEOGRAPHY::STGeomFromText('Point(' + CAST(@ToLongitude AS VARCHAR(32)) + ' ' + CAST(@ToLatitude AS VARCHAR(32)) + ')',4326)

	SELECT ROUND( @FromPoint.STDistance(@ToPoint),0,0)
END
GO


