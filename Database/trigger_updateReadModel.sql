

-- =============================================
-- Author:		Haviv Tzin
-- Description:	Update SpatialObjectReadModel table
-- =============================================
CREATE TRIGGER [dbo].[updateReadModel] 
   ON   [dbo].[SpatialObject]
   AFTER  UPDATE
AS 
BEGIN

	SET NOCOUNT ON;


	UPDATE [dbo].[SpatialObjectReadModel]
	   SET [Name] = i.[Name]
		  ,[Description] = i.[Description]
		  ,[Latitude] = loc.Latitude
		  ,[Longitude] = loc.Longitude
	FROM inserted  i
	INNER JOIN [dbo].[Location] loc ON loc.Id = i.LocationId
	 WHERE [dbo].[SpatialObjectReadModel].Id = i.[Id]



END
GO

ALTER TABLE [dbo].[SpatialObject] ENABLE TRIGGER [updateReadModel]
GO


