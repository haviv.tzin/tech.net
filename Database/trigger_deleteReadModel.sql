
-- =============================================
-- Author:		Haviv Tzin
-- Description:	delete SpatialObjectReadModel table
-- =============================================
CREATE TRIGGER [dbo].[deleteReadModel] 
   ON   [dbo].[SpatialObject]
   AFTER  DELETE
AS 
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [dbo].[SpatialObjectReadModel]
		  WHERE Id IN(SELECT deleted.id FROM deleted)

END
GO

ALTER TABLE [dbo].[SpatialObject] ENABLE TRIGGER [deleteReadModel]
GO


