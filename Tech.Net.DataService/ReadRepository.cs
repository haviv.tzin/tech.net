﻿using System.Collections.Generic;
using System.Linq;
using Tech.Net.Common;

namespace Tech.Net.DataService
{
    public class ReadRepository : IReadRepository,IIocPerWebRequest
    {
        /// <summary>
        /// GetMapBoundsObjects
        /// </summary>
        /// <param name="northEast"></param>
        /// <param name="southWest"></param>
        /// <returns></returns>
        public List<SpatialObjectReadModel> GetMapBoundsObjects(Location northEast, Location southWest)
        {
            using (var context = new TechNetDBEntities())
            {
                var relevantObjIdsList = context.Locations.Where(location =>
                        location.Latitude >= southWest.Latitude && location.Latitude <= northEast.Latitude &&
                        location.Longitude >= southWest.Longitude && location.Longitude <= northEast.Longitude)
                    .Select(a => a.SpatialObjects.Select(b => b.Id)).ToList();


                var objIdsList = new List<int>();
                relevantObjIdsList.ForEach(objIdsList.AddRange);
                objIdsList = objIdsList.Distinct().ToList();

                var retVal = context.SpatialObjectReadModels.Where(s => objIdsList.Contains(s.Id)).ToList();

                return retVal;
            }
        }

        /// <summary>
        /// GetGeographyDistance
        /// </summary>
        /// <param name="fromPoint"></param>
        /// <param name="toPoint"></param>
        /// <returns></returns>
        public long GetGeographyDistance(Location fromPoint, Location toPoint)
        {
            using (var context = new TechNetDBEntities())
            {
                var retVal = context.sp_GetGeographyDistance(fromPoint.Latitude, fromPoint.Longitude, toPoint.Latitude,
                    toPoint.Longitude);

                return (long) (retVal?.Single() ?? 0.0);
            }
        }

        /// <summary>
        /// Get All Objects - in real application not wise to get all better to get subset using fillter
        /// for this demo the dataset is small so no worries :)
        /// </summary>
        /// <returns></returns>
        public List<SpatialObjectReadModel> GetAllObjects()
        {
            using (var context = new TechNetDBEntities())
            {
                var retVal = context.SpatialObjectReadModels.ToList();
                return retVal;
            }
        }
    }
}
