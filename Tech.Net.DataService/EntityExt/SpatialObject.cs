﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tech.Net.DataService
{
    public partial class SpatialObject
    {
        /// <summary>
        /// Copy object properties
        /// </summary>
        /// <param name="sourceObject"></param>
        public void CopyFrom(SpatialObject sourceObject)
        {
            Id = sourceObject.Id;
            LocationId = sourceObject.LocationId;
            Name = sourceObject.Name;
            Description = sourceObject.Description;

            if (sourceObject.Location != null) { 
            Location=new Location{Id = sourceObject.LocationId, Latitude = sourceObject.Location.Latitude,Longitude=sourceObject.Location.Longitude};
            }

        }

        /// <summary>
        /// Copy object properties
        /// </summary>
        /// <param name="sourceObject"></param>
        public void CopyFrom(SpatialObjectReadModel sourceObject)
        {
            Id = sourceObject.Id;
            Name = sourceObject.Name;
            Description = sourceObject.Description;
            Location = new Location {  Latitude = sourceObject.Latitude, Longitude = sourceObject.Longitude };
        }

    }
}
