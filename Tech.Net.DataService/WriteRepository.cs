﻿using System.Data.Entity.Migrations;
using System.Linq;
using Tech.Net.Common;

namespace Tech.Net.DataService
{
    public class WriteRepository: IWriteRepository, IIocPerWebRequest
    {
        /// <summary>
        /// AddNewSpatialObject
        /// </summary>
        /// <param name="spatialObject"></param>
        public SpatialObject AddNewSpatialObject(SpatialObject spatialObject)
        {
            using (var context = new TechNetDBEntities())
            {
                context.SpatialObjects.Add(spatialObject);
                context.SaveChanges();
                var dtoObject = new SpatialObject();
                dtoObject.CopyFrom(spatialObject);
                return dtoObject;
            }
        }

        /// <summary>
        /// UpdateSpatialObject
        /// </summary>
        /// <param name="spatialObject"></param>
        /// <returns></returns>
        public SpatialObject UpdateSpatialObject(SpatialObject spatialObject)
        {
            using (var context = new TechNetDBEntities())
            {
                var contextObject = context.SpatialObjects.Include("Location").Single(a => a.Id == spatialObject.Id);

                contextObject.Description = spatialObject.Description;
                if (spatialObject.Location != null)
                {
                    contextObject.Location.Latitude = spatialObject.Location.Latitude;
                    contextObject.Location.Longitude = spatialObject.Location.Longitude;
                }
                context.SaveChanges();

                var dtoObject = new SpatialObject();
                dtoObject.CopyFrom(contextObject);
                return dtoObject;
            }
        }
    }
}
