﻿namespace Tech.Net.DataService
{
    public interface IWriteRepository
    {
        SpatialObject AddNewSpatialObject(SpatialObject spatialObject);

        SpatialObject UpdateSpatialObject(SpatialObject spatialObject);
    }
}
