﻿using System.Collections.Generic;

namespace Tech.Net.DataService
{
    public interface IReadRepository
    {
        /// <summary>
        /// Get Map Bounds Objects
        /// </summary>
        /// <param name="northEast"></param>
        /// <param name="southWest"></param>
        /// <returns></returns>
        List<SpatialObjectReadModel> GetMapBoundsObjects(Location northEast, Location southWest);

        /// <summary>
        /// Get Geography Distance between two objects
        /// </summary>
        /// <param name="fromPoint"></param>
        /// <param name="toPoint"></param>
        /// <returns></returns>
        long GetGeographyDistance(Location fromPoint, Location toPoint);

        /// <summary>
        /// GetAllObjects
        /// </summary>
        /// <returns></returns>
        List<SpatialObjectReadModel> GetAllObjects();
    }
}